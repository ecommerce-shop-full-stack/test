const express = require("express");
const {
  createBlog,
  updateBlog,
  getAllBlog,
  getBlogById,
  deleteBlog,
  likeBlog,
  dislikeBlog,
} = require("../controllers/blogController");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const router = express.Router();

//
router.post("/", authenMiddleware, isAdmin, createBlog);
router.get("/", authenMiddleware, isAdmin, getAllBlog);

router.put("/likes", authenMiddleware, likeBlog);
router.put("/dislikes", authenMiddleware, dislikeBlog);

router.put("/:id", authenMiddleware, isAdmin, updateBlog);
router.get("/:id", authenMiddleware, isAdmin, getBlogById);
router.delete("/:id", authenMiddleware, isAdmin, deleteBlog);

module.exports = router;
