const express = require("express");
const {
  createColor,
  getAllColor,
  updateColor,
  deleteColor,
  getColorById,
} = require("../controllers/colorController");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/", authenMiddleware, isAdmin, createColor);
router.get("/", authenMiddleware, isAdmin, getAllColor);
router.get("/:id", authenMiddleware, isAdmin, getColorById);
router.put("/:id", authenMiddleware, isAdmin, updateColor);
router.delete("/:id", authenMiddleware, isAdmin, deleteColor);

module.exports = router;
