const express = require("express");
const {
  createBlogCategory,
  getAllBlogCategory,
  updateBlogCategory,
  deleteBlogCategory,
  getBlogCategoryById,
} = require("../controllers/blogCategoryController");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/", authenMiddleware, isAdmin, createBlogCategory);
router.get("/", authenMiddleware, isAdmin, getAllBlogCategory);
router.get("/:id", authenMiddleware, isAdmin, getBlogCategoryById);
router.put("/:id", authenMiddleware, isAdmin, updateBlogCategory);
router.delete("/:id", authenMiddleware, isAdmin, deleteBlogCategory);

module.exports = router;
