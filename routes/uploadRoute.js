const express = require("express");
const {
  uploadImages,
  deleteImages,
} = require("../controllers/uploadController");
const { isAdmin, authenMiddleware } = require("../middlewares/authMiddleware");
const {
  uploadPhoto,
  productImgResize,
} = require("../middlewares/uploadImages");
const router = express.Router();

router.post(
  "/",
  authenMiddleware,
  isAdmin,
  uploadPhoto.array("images", 10),
  productImgResize,
  uploadImages
);

router.delete("/delete-img/:id", authenMiddleware, isAdmin, deleteImages);

module.exports = router;
