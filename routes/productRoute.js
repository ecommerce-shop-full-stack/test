const express = require("express");
const {
  createProduct,
  getProductById,
  getAllProduct,
  updateProduct,
  deleteProduct,
  addToWishlist,
  productRating,
} = require("../controllers/productController");
const { isAdmin, authenMiddleware } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/", authenMiddleware, isAdmin, createProduct);
router.get("/:id", getProductById);
router.put("/wishlist", authenMiddleware, addToWishlist);
router.put("/rating", authenMiddleware, productRating);

router.get("/", getAllProduct);
router.put("/:id", authenMiddleware, isAdmin, updateProduct);
router.delete("/:id", authenMiddleware, isAdmin, deleteProduct);

module.exports = router;
