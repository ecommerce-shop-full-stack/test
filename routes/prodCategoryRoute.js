const express = require("express");
const {
  createProdCategory,
  getAllProdCategory,
  getProdCategoryById,
  updateProdCategory,
  deleteProdCategory,
} = require("../controllers/prodCategoryController");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/", authenMiddleware, isAdmin, createProdCategory);
router.get("/", authenMiddleware, isAdmin, getAllProdCategory);
router.get("/:id", authenMiddleware, isAdmin, getProdCategoryById);
router.put("/:id", authenMiddleware, isAdmin, updateProdCategory);
router.delete("/:id", authenMiddleware, isAdmin, deleteProdCategory);

module.exports = router;
