const express = require("express");
const {
  createEnquiry,
  getAllEnquiry,
  updateEnquiry,
  deleteEnquiry,
  getEnquiryById,
} = require("../controllers/enqController");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/", createEnquiry);
router.get("/", authenMiddleware, isAdmin, getAllEnquiry);
router.get("/:id", authenMiddleware, isAdmin, getEnquiryById);
router.put("/:id", authenMiddleware, isAdmin, updateEnquiry);
router.delete("/:id", authenMiddleware, isAdmin, deleteEnquiry);

module.exports = router;
