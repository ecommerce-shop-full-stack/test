const express = require("express");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const {
  createCoupon,
  getAllCoupon,
  getCouponById,
  updateCoupon,
  deleteCoupon,
} = require("../controllers/couponController");
const router = express.Router();

router.post("/", authenMiddleware, isAdmin, createCoupon);
router.get("/", authenMiddleware, isAdmin, getAllCoupon);
router.get("/:id", authenMiddleware, isAdmin, getCouponById);
router.put("/:id", authenMiddleware, isAdmin, updateCoupon);
router.delete("/:id", authenMiddleware, isAdmin, deleteCoupon);

module.exports = router;
