const express = require("express");
const {
  createUser,
  loginUser,
  getAllUser,
  getUser,
  deleteUser,
  updateUser,
  blockUser,
  unblockUser,
  handleRefreshToken,
  logoutUser,
  updatePassword,
  forgotPasswordToken,
  resetPassword,
  loginAdmin,
  getWishlist,
  saveAddress,
  addToCart,
  getUserCart,
  emptyCart,
  applyCoupon,
  createOrder,
  getOrders,
  updateOrderStatus,
} = require("../controllers/userController");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/register", createUser);
router.post("/forgot-password", forgotPasswordToken);
router.post("/login", loginUser);
router.post("/admin-login", loginAdmin);
router.post("/cart", authenMiddleware, addToCart);
router.post("/cart/apply-coupon", authenMiddleware, applyCoupon);
router.post("/cart/create-order", authenMiddleware, createOrder);

router.get("/logout", logoutUser);
router.get("/all-users", getAllUser);
router.get("/cart/order", authenMiddleware, getOrders);
router.get("/cart", authenMiddleware, getUserCart);
router.get("/refresh", handleRefreshToken);
router.get("/wishlist", authenMiddleware, getWishlist);
router.get("/:id", authenMiddleware, isAdmin, getUser);

router.delete("/empty-cart", authenMiddleware, emptyCart);
router.delete("/:id", deleteUser);

router.put("/reset-password/:token", resetPassword);
router.put("/password", authenMiddleware, updatePassword);
router.put("/edit-user", authenMiddleware, updateUser);
router.put("/save-address", authenMiddleware, saveAddress);
router.put("/block-user/:id", authenMiddleware, isAdmin, blockUser);
router.put("/unblock-user/:id", authenMiddleware, isAdmin, unblockUser);
router.put(
  "/order/update-order/:id",
  authenMiddleware,
  isAdmin,
  updateOrderStatus
);

module.exports = router;
