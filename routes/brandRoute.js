const express = require("express");
const {
  createBrand,
  getAllBrand,
  updateBrand,
  deleteBrand,
  getBrandById,
} = require("../controllers/BrandController");
const { authenMiddleware, isAdmin } = require("../middlewares/authMiddleware");
const router = express.Router();

router.post("/", authenMiddleware, isAdmin, createBrand);
router.get("/", authenMiddleware, isAdmin, getAllBrand);
router.get("/:id", authenMiddleware, isAdmin, getBrandById);
router.put("/:id", authenMiddleware, isAdmin, updateBrand);
router.delete("/:id", authenMiddleware, isAdmin, deleteBrand);

module.exports = router;
