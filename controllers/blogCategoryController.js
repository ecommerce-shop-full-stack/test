const BlogCategory = require("../models/blogCategoryModel");
const asyncHandler = require("express-async-handler");
const validateMongoDbId = require("../utils/validateMongodbId");

const createBlogCategory = asyncHandler(async (req, res) => {
  try {
    const newBlogCategory = await BlogCategory.create(req.body);
    res.json(newBlogCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const getAllBlogCategory = asyncHandler(async (req, res) => {
  try {
    const getAllBlogCate = await BlogCategory.find();
    res.json(getAllBlogCate);
  } catch (error) {
    throw new Error(error);
  }
});

const getBlogCategoryById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const getBlogCateById = await BlogCategory.findById(id);
    res.json(getBlogCateById);
  } catch (error) {
    throw new Error(error);
  }
});

const updateBlogCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const updatedBlogCate = await BlogCategory.findByIdAndUpdate(
      id,
      {
        title: req?.body?.title,
      },
      {
        new: true,
      }
    );
    res.json({ msg: "Blog Category has been updated", updatedBlogCate });
  } catch (error) {
    throw new Error(error);
  }
});

const deleteBlogCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const deletedBlogCate = await BlogCategory.findByIdAndDelete(id);
    res.json({
      message: "Blog Category has been deleted",
    });
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createBlogCategory,
  getAllBlogCategory,
  getBlogCategoryById,
  updateBlogCategory,
  deleteBlogCategory,
};
