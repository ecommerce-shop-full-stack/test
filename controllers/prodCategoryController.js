const ProdCategory = require("../models/prodCategoryModel");
const asyncHandler = require("express-async-handler");
const validateMongoDbId = require("../utils/validateMongodbId");

const createProdCategory = asyncHandler(async (req, res) => {
  try {
    const newProdCategory = await ProdCategory.create(req.body);
    res.json(newProdCategory);
  } catch (error) {
    throw new Error(error);
  }
});

const getAllProdCategory = asyncHandler(async (req, res) => {
  try {
    const getAllProdCate = await ProdCategory.find();
    res.json(getAllProdCate);
  } catch (error) {
    throw new Error(error);
  }
});

const getProdCategoryById = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const getProdCateById = await ProdCategory.findById(id);
    res.json(getProdCateById);
  } catch (error) {
    throw new Error(error);
  }
});

const updateProdCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const updatedProdCate = await ProdCategory.findByIdAndUpdate(
      id,
      {
        title: req?.body?.title,
      },
      {
        new: true,
      }
    );
    res.json({ msg: "Prod Category has been updated", updatedProdCate });
  } catch (error) {
    throw new Error(error);
  }
});

const deleteProdCategory = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const deletedCate = await ProdCategory.findByIdAndDelete(id);
    res.json({
      message: "ProdCategory has been deleted",
    });
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = {
  createProdCategory,
  getAllProdCategory,
  getProdCategoryById,
  updateProdCategory,
  deleteProdCategory,
};
